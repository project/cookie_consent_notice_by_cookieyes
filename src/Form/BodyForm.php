<?php

namespace Drupal\cookieyes_scripts\Form;

use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;
use Symfony\Component\HttpFoundation\Request;

/**
 * The admin configuration setting form.
 */
class BodyForm extends ConfigFormBase {

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'cookieyes_settings';
  }

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames() {
    return ['cookieyes_scripts.settings'];
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state, Request $request = NULL) {
    $settings = $this->config('cookieyes_scripts.settings')->get();

    $form['cookieyes'] = [
      '#type' => 'fieldset',
      '#title' => $this->t('Add CookieYes Script'),
      '#description' => $this->t('All the defined scripts in this section will be added in the <code><body></code> tag.'),
    ];

    $form['cookieyes']['enable'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Enable CookieYes'),
      '#default_value' => $settings['enable'] ?? TRUE,
    ];

    $form['cookieyes']['scripts'] = [
      '#type' => 'textarea',
      '#title' => $this->t('GDPR Script'),
      '#default_value' => $settings['scripts'] ?? '',
      '#rows' => 10,
    ];

    return parent::buildForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $this->config('cookieyes_scripts.settings')
      ->set('scripts', $form_state->getValue('scripts'))
      ->set('enable', $form_state->getValue('enable'))
      ->save();

    parent::submitForm($form, $form_state);
  }

}
